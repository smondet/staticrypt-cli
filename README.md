`hscrypt`
=========

Command-line script that creates encrypted web-pages with a password form
(a bit like
[`robinmoisson/staticrypt`](https://github.com/robinmoisson/staticrypt))
it uses 
`gpg` to encrypt and [`OpenPGP.js`](https://github.com/openpgpjs/openpgpjs)
to decrypt.
Passphrases can be provided in a text-input or with the URL selector (`#...`).

Install
-------

The script is generated into `_build/hscrypt` with `make`, to install it:

    make install BINDIR=$HOME/bin

Usage
-----

You can use a file:

    printf 'Hello\n===\n\nThis *is* a `test`\n' | pandoc -s -o /tmp/test.html
    hscrypt -f /tmp/test.html -p test-pass-phrase -o /tmp/hscrypt-test.html
    
Open <file:///tmp/hscrypt-test.html> or <file:///tmp/hscrypt-test.html#test-pass-phrase>.

or directly a string:

    hscrypt -s "This is another test" -p testtest -o /tmp/hscrypt-test2.html

